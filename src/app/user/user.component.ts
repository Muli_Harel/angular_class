import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import {User} from './user';

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user'] // הגדרנו תכונה שנקראת user
                  // אפשר להוסיף עוד תכונה באמצעות פסיק 
})
export class UserComponent implements OnInit {

user:User; // השמה, יוזר הוא מסוג יוזר
@Output() deleteEvent = new EventEmitter<User>(); 
@Output() editEvent = new EventEmitter<User>(); 


isEdit: Boolean = false;
editButtonText = "Edit";

constructor() { }

sendDelete(){
  this.deleteEvent.emit(this.user); 

}

toggleEdit(){
  this.isEdit = !this.isEdit;
  this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  if(!this.isEdit){
    this.editEvent.emit(this.user);
  }

}

  ngOnInit() {
  }

}