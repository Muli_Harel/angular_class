import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map'; // import for map function 
import 'rxjs/add/operator/delay'; // מוסיפים דיליי בשביל לראות את הספינר בצורה יותר ארוכה

@Injectable()
export class UsersService {

 //private _url = 'http://jsonplaceholder.typicode.com/users'; //we remove this line because we want to read from firebase

usersObservable;

getUsers(){
  this.usersObservable = this.af.database.list('/users'); // if we choose object (insted of list) the syntax will be:  object('/users/1')    ('users' is the name of table in firebase)
  return this.usersObservable;
 // return this._http.get(this._url).map(res => res.json()).delay(500)     // res is just the name, like result  // delay is for the spinner // we don't want to read from url only from firebase
  }

addUser (user){
  this.usersObservable.push(user);   
}

updateUser(user){
  let userKey = user.$key;
  let userData = {name:user.name, email:user.email};
  this.af.database.object('/users/' + userKey).update(userData);

}

deleteUser(user){
  let userKey = user.$key;
  this.af.database.object('/users/' + userKey).remove();
}


  constructor(private af:AngularFire) { } // לא קריטי להוסיף קו תחתון לפני השם , זאת לא דרישה של סינטקס

}