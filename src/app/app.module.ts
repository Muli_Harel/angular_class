import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AngularFireModule } from 'angularfire2'; // for firebase

 var firebaseConfig = {
    apiKey: "AIzaSyDr5kRS9VrSiRFNYSJ3hd3KsHQcSefRWXo",
    authDomain: "angular-class-2683a.firebaseapp.com",
    databaseURL: "https://angular-class-2683a.firebaseio.com",
    storageBucket: "angular-class-2683a.appspot.com",
    messagingSenderId: "721990849394"
}


const appRoutes:Routes= [
  {path: 'users', component:UsersComponent},
  {path: 'posts', component:PostsComponent},
  {path: '', component:UsersComponent},
  {path: '**', component:PageNotFoundComponent},
  ]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    UserFormComponent,
    PageNotFoundComponent   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig) // for firebase
  ],
  providers: [UsersService], //לא לשכוח להוסיף את זה בשביל שיעבוד
  bootstrap: [AppComponent]
})
export class AppModule { }